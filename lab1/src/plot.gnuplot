set terminal pngcairo
set output 'output/my_plot.png'

set title "Графики зависимости времени передачи\n сообщения и размера передаваемого сообщения"
set xlabel "Размер сообщения (МБ)"
set ylabel "Среднее время передачи сообщения (Сек)"

set xrange [10:100]
set yrange [0:0.15]
set key left

plot '1.csv' using 1:2 with lines linestyle 1 lw 2 title 'Оперативная память', \
   '2.csv' using 1:2 with lines linestyle 2 lw 2 title 'Внутрисистемная шина', \
   '3.csv' using 1:2 with lines linestyle 3 lw 2 title 'Сеть связи между ЭМ'

unset output