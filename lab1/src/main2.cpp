#include <iostream>
#include <iomanip>
#include <mpi.h>
#include <vector>

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);

    int rank, commsize;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &commsize);

    // int dest = (rank + 1) % commsize;
    // int source = (rank + commsize - 1) % commsize;

    int step_size = 10, stop_size = 100;
    int reps = 10;

    MPI_Request send_request, recv_request;
    for(int size = 10; size <= stop_size; size += step_size) {
        const int chunksize = size * 1024 * 1024;
        std::vector<int> send_buffer(chunksize, rank);
        std::vector<int> recv_buffer(chunksize);

        double total_time = 0.0;
        for(int i = 0; i < reps; i++) {
            MPI_Barrier(MPI_COMM_WORLD);

            if(rank == 0) {
                double send_start_time = MPI_Wtime();
                MPI_Isend(send_buffer.data(), chunksize, MPI_INT, 1, 0, MPI_COMM_WORLD, &send_request);
                MPI_Wait(&send_request, MPI_STATUS_IGNORE);
                total_time += MPI_Wtime() - send_start_time;
            }
            else {
                double recv_start_time = MPI_Wtime();
                MPI_Irecv(recv_buffer.data(), chunksize, MPI_INT, 0, 0, MPI_COMM_WORLD, &recv_request);
                MPI_Wait(&recv_request, MPI_STATUS_IGNORE);
                total_time += MPI_Wtime() - recv_start_time;
            }
        }

        double avg_time = total_time / double(reps);

        double global_avg_time = 0.0;
        MPI_Reduce(&avg_time, &global_avg_time, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

        if (rank == 0) {
            global_avg_time /= commsize;
            std::cout << size << " " << std::fixed << std::setprecision(20) << global_avg_time << std::endl;
        }
    }

    MPI_Finalize();
    return 0;
}
