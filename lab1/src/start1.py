from os import system, chdir, listdir, getcwd
import subprocess
from time import sleep
import shutil

if shutil.which('sbatch'):
    system("mpic++ -o main2 -Wall ./main2.cpp")
    subprocess.run(["sbatch", "task1.slurm"])
    subprocess.run(["sbatch", "task2.slurm"])
    subprocess.run(["sbatch", "task3.slurm"])
    sleep(60)
    files = listdir(getcwd())
    out_files = sorted(file for file in files if file.endswith('.out'))

    tmp = ""
    with open(out_files[0], "r") as file:
        tmp = file.readlines()
    with open("1.csv", "w+") as file:
        file.writelines(tmp)

    with open(out_files[1], "r") as file:
        tmp = file.readlines()
    with open("2.csv", "w+") as file:
        file.writelines(tmp)

    with open(out_files[2], "r") as file:
        tmp = file.readlines()
    with open("3.csv", "w+") as file:
        file.writelines(tmp)

    system("gnuplot plot.gnuplot")
    system("rm main2")
    for o in out_files:
        system(f"rm {o}")
    # for i in range(1, 4):
    #     system(f"rm {i}.csv")
    system("rm -rf __pycache__")
else:
    print("sbatch is not installed")

# sbatch task.slurm
# squeue
# scancel ID