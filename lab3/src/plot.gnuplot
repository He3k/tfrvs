set terminal png size 1200, 900 font 'Verdana, 14'
set title "Лабораторная работа 3. График зависимости времени работы от количества ЭМ" font "Helvetica Bold, 18"
set output 'output/lab3.png'

set xlabel "Количество ЭМ (шт)"
set ylabel "Время работы алгоритма (сек)"

set xrange [1:64]
set yrange [0:*]
set grid
set xtics ("2" 2, "4" 4, "8" 8, "16" 16, "32" 32, "64" 64)
set key left
set key at -10,1.8

plot 'bench.csv' using 1:2 with lines linestyle 1 lw 2 title 'Время работы на n машинах'

unset output