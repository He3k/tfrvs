#include <iostream>
#include <vector>
#include <iomanip>
#include <cmath>
#include <chrono>
#include <map>
#include "json.hpp"

bool toOutputMatrix = true;
bool toOutputStrategies = true;
bool toOutputGameCost = true;
bool toOutputIterations = true;
bool toOutputTimeSpent = true;
double epsilon;

class StrategyMatrix {
public:
    StrategyMatrix(std::vector<std::vector<int>> matrix) : c(matrix), i(0), a(matrix.size(), 0), b(matrix[0].size(), 0), p(matrix[0].size(), 0), q(matrix.size(), 0) {}

    void iterateB() {
        int maxIndex = maxValIndex(a);
        for (size_t j = 0; j < c[maxIndex].size(); ++j) {
            b[j] += c[maxIndex][j];
        }
        q[maxIndex]++;
    }

    void iterateA() {
        int minIndex = minValIndex(b);
        for (size_t row = 0; row < c.size(); ++row) {
            a[row] += c[row][minIndex];
        }
        p[minIndex]++;
    }

    double gameCostMin() const {
        int min = b[0];
        for (int val : b) {
            if (min > val) {
                min = val;
            }
        }
        return static_cast<double>(min) / static_cast<double>(i);
    }

    double gameCostMax() const {
        int max = a[0];
        for (int val : a) {
            if (max < val) {
                max = val;
            }
        }
        return static_cast<double>(max) / static_cast<double>(i);
    }

    double gameCostAvg() const {
        return (gameCostMax() + gameCostMin()) / 2;
    }

    int Iterations() const {
        return i;
    }

    std::vector<double> dispatcherStrategies() const {
        std::vector<double> result;
        for (int pv : p) {
            result.push_back(static_cast<double>(pv) / static_cast<double>(i));
        }
        return result;
    }

    std::vector<double> ccStrategies() const {
        std::vector<double> result;
        for (int qv : q) {
            result.push_back(static_cast<double>(qv) / static_cast<double>(i));
        }
        return result;
    }

    void Iterate() {
        i++;
        iterateB();
        iterateA();
    }

private:
    std::vector<std::vector<int>> c;
    int i;
    std::vector<int> a;
    std::vector<int> b;
    std::vector<int> p;
    std::vector<int> q;

    int minValIndex(const std::vector<int>& arr) const {
        int index = 0;
        for (size_t j = 0; j < arr.size(); ++j) {
            if (arr[j] < arr[index]) {
                index = j;
            }
        }
        return index;
    }

    int maxValIndex(const std::vector<int>& arr) const {
        int index = 0;
        for (size_t j = 0; j < arr.size(); ++j) {
            if (arr[j] > arr[index]) {
                index = j;
            }
        }
        return index;
    }
};

void parseArgs(int argc, char* argv[], int& n, int& c1, int& c2, int& c3, double& epsilon) {
    int argIndex = 1;

    while (argIndex < argc) {
        std::string arg = argv[argIndex];

        if (arg == "-eps") {
            epsilon = std::stod(argv[argIndex + 1]);
            argIndex += 2;
        } else {
            n = std::stoi(argv[argIndex]);
            c1 = std::stoi(argv[argIndex + 1]);
            c2 = std::stoi(argv[argIndex + 2]);
            c3 = std::stoi(argv[argIndex + 3]);
            argIndex += 4;
        }
    }
}

void validateArgs(int n, int c1, int c2, int c3, double epsilon) {
    std::vector<std::string> defects;

    if (n <= 0) {
        defects.push_back("  - n must be positive");
    }

    if (c1 < 1 || c1 > 3) {
        defects.push_back("  - c_1 must be in range {1, 2, 3}");
    }

    if (c2 < 4 || c2 > 6) {
        defects.push_back("  - c_2 must be in range {4, 5, 6}");
    }

    if (c3 < 4 || c3 > 6) {
        defects.push_back("  - c_3 must be in range {4, 5, 6}");
    }

    if (epsilon <= 0 || epsilon >= 1) {
        defects.push_back("  - eps must be in range (0; 1)");
    }

    if (!defects.empty()) {
        std::cerr << "Following defects in arguments were found:\n";
        for (const auto& defect : defects) {
            std::cerr << defect << '\n';
        }
        exit(EXIT_FAILURE);
    }
}

void generateMatrix(std::vector<std::vector<int>>& C, const int n, const int c1, const int c2, const int c3) {
    C.resize(n, std::vector<int>(n, 0));

    int max = 0;
    for (int i = 0; i < n; ++i) {
        int j = 0;
        for (; j <= i; ++j) {
            C[i][j] = j * c1 + (i - j) * c2;
            if (C[i][j] > max) {
                max = C[i][j];
            }
        }
        for (; j < n; ++j) {
            C[i][j] = i * c2 + (j - i) * c3;
            if (C[i][j] > max) {
                max = C[i][j];
            }
        }
    }

    if (toOutputMatrix) {
        int colWidth = static_cast<int>(std::log10(max)) + 1;
        for (const auto& row : C) {
            for (int num : row) {
                std::cout << std::setw(colWidth) << num << ' ';
            }
            std::cout << '\n';
        }
    }
}

void iterateMatrix(StrategyMatrix& sm, bool toOutputMatrix, bool toOutputStrategies, bool toOutputGameCost, bool toOutputIterations, bool toOutputTimeSpent, double epsilon, int n) {
    auto start = std::chrono::high_resolution_clock::now();
    sm.Iterate();

    while (sm.gameCostMax() - sm.gameCostMin() > epsilon) {
        sm.Iterate();
    }

    auto end = std::chrono::high_resolution_clock::now();
    double elapsedSeconds = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1'000'000.0;

    std::map<std::string, nlohmann::json> results;

    if (toOutputStrategies) {
        results["DispatcherStrategies"] = sm.dispatcherStrategies();
        results["DcsStrategies"] = sm.ccStrategies();
        std::cout << "DispStrategies: " << results["DispatcherStrategies"] << "\nDcsStrategies: " <<   results["DcsStrategies"] << std::endl;
    }

    if (toOutputGameCost) {
        // results["GameCostMin"] = sm.gameCostMin();
        results["GameCostAvg"] = sm.gameCostAvg();
        // results["GameCostMax"] = sm.gameCostMax();
        // std::cout << "GameCostMin: " << results["GameCostMin"] << "\nGameCostAvg: " <<   results["GameCostAvg"] << "\nGameCostMax: " <<   results["GameCostMax"] << std::endl;
        std::cout << "GameCostAvg: " << results["GameCostAvg"] << std::endl;

    }

    if (toOutputIterations) {
        results["IterationsSpent"] = sm.Iterations();
        std::cout << "IterationsSpent: " << results["IterationsSpent"] << std::endl;
    }

    if (toOutputTimeSpent) {
        results["TimeSpentSeconds"] = elapsedSeconds;
        std::cout << "TimeSpentSeconds: " << results["TimeSpentSeconds"] << std::endl;
    }

    if (!results.empty()) {
        std::cout << n << ";" << elapsedSeconds << std::endl;
    }
}

int main(int argc, char* argv[]) {
    int n, c1, c2, c3;
    double epsilon = 0.01;

    parseArgs(argc, argv, n, c1, c2, c3, epsilon);
    validateArgs(n, c1, c2, c3, epsilon);

    std::vector<std::vector<int>> matrix;
    generateMatrix(matrix, n, c1, c2, c3);
    StrategyMatrix sm(matrix);
    iterateMatrix(sm, toOutputMatrix, toOutputStrategies, toOutputGameCost, toOutputIterations, toOutputTimeSpent, epsilon, n);

    return 0;
}
