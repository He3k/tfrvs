import subprocess
from os import system, chdir

system("g++ main.cpp -o main")
numbers = [2**i for i in range(1,7)]
reps = 10
total = ""
for i in numbers:
    for rep in range(0, reps):
        res = subprocess.run(["./main", "-eps", "0.01", str(i), "3", "4", "5"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout = res.stdout.decode('utf-8')
        stderr = res.stderr.decode('utf-8')
        line = stdout.split("\n")[-2].split(";")
        if rep == 0:
            accum = [line[0], float(line[1])/reps]
        else:
            accum[1] += float(line[1])/reps
    with open("bench.csv", "a+") as file:
        file.write(f"{accum[0]} {str(accum[1])}\n")
    total += stdout
with open("output/results.csv", "w+") as file:
    file.writelines(total)
system("gnuplot plot.gnuplot")
system("rm main")
system("rm bench.csv")
