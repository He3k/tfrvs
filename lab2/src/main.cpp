#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <cassert>
#include <iomanip>
#include <stdexcept>

struct Task
{
    int R;
    int T;
};

int TotalTime(const std::vector<std::vector<Task>> &taskLevels)
{
    int tt = 0;
    for (const auto &taskLevel : taskLevels)
    {
        tt += taskLevel[0].T;
    }
    return tt;
}

double TimeBound(const std::vector<std::vector<Task>> &taskLevels, int n)
{
    int tavg = 0;
    for (const auto &taskLevel : taskLevels)
    {
        for (const auto &task : taskLevel)
        {
            tavg += task.T * task.R;
        }
    }
    return static_cast<double>(tavg) / static_cast<double>(n);
}

double FnDeviation(const std::vector<std::vector<Task>> &taskLevels, int n)
{
    return (static_cast<double>(TotalTime(taskLevels)) - TimeBound(taskLevels, n)) / TimeBound(taskLevels, n);
}

std::vector<std::vector<Task>> NFDH(const std::vector<Task> &records, int n)
{
    std::vector<Task> oversize;
    for (const auto &record : records)
    {
        if (record.R > n)
        {
            oversize.push_back(record);
        }
    }

    if (!oversize.empty())
    {
        throw std::runtime_error("Only having " + std::to_string(n) + " processes, while tasks require more");
    }

    size_t tasksLength = records.size();
    std::vector<std::vector<Task>> taskLevels;

    if (tasksLength == 0)
    {
        return taskLevels;
    }

    size_t j = 0;
    for (size_t i = 0;; ++i)
    {
        int takenProc = 0;
        taskLevels.push_back({});
        while (true)
        {
            takenProc += records[j].R;
            if (takenProc > n)
            {
                break;
            }
            taskLevels[i].push_back(records[j]);
            ++j;
            if (j >= tasksLength)
            {
                return taskLevels;
            }
        }
    }
}

std::vector<std::vector<Task>> FFDH(const std::vector<Task> &records, int n)
{
    std::vector<Task> oversize;
    for (const auto &record : records)
    {
        if (record.R > n)
        {
            oversize.push_back(record);
        }
    }

    if (!oversize.empty())
    {
        throw std::runtime_error("Only having " + std::to_string(n) + " processes, while tasks require more");
    }

    std::vector<std::vector<Task>> taskLevels;
    std::vector<int> levelTakenProc;

    for (const auto &task : records)
    {
        bool taskAllocated = false;
        for (size_t i = 0; i < taskLevels.size(); ++i)
        {
            if (levelTakenProc[i] + task.R <= n)
            {
                levelTakenProc[i] += task.R;
                taskLevels[i].push_back(task);
                taskAllocated = true;
                break;
            }
        }

        if (!taskAllocated)
        {
            taskLevels.push_back({task});
            levelTakenProc.push_back(task.R);
        }
    }

    return taskLevels;
}

void TestFFDH()
{
    std::vector<Task> records = {{3, 6}, {5, 3}, {4, 2}, {6, 2}, {2, 2}, {7, 1}};
    int n = 10;

    try
    {
        auto packed = FFDH(records, n);
        int tt = TotalTime(packed);
        assert(tt == 9 && "FFDH pass through test case should give 9 total time");
        std::cout << "FFDH Test passed successfully." << std::endl;
    }
    catch (const std::runtime_error &e)
    {
        std::cerr << "Error: " << e.what() << std::endl;
    }
}

void TestNFDH()
{
    std::vector<Task> records = {{3, 6}, {5, 3}, {4, 2}, {6, 2}, {2, 2}, {7, 1}};
    int n = 10;

    try
    {
        auto packed = NFDH(records, n);
        int tt = TotalTime(packed);
        assert(tt == 10 && "NFDH pass through test case should give 10 total time");
        std::cout << "NFDH Test passed successfully." << std::endl;
    }
    catch (const std::runtime_error &e)
    {
        std::cerr << "Error: " << e.what() << std::endl;
    }
}

struct AlgoEfficiency
{
    double PerformanceTime;
    int ScheduleTotalTime;
    double ScheduleDeviation;
    std::vector<std::vector<Task>> Schedule;
};

AlgoEfficiency outputAlgoEfficiency(const std::vector<Task> &records, int rn, std::vector<std::vector<Task>> (*algo)(const std::vector<Task> &, int))
{
    auto start = std::chrono::high_resolution_clock::now();
    auto taskLevels = algo(records, rn);
    auto end = std::chrono::high_resolution_clock::now();

    return {
        static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()) / 1'000'000,
        TotalTime(taskLevels),
        FnDeviation(taskLevels, rn),
        taskLevels};
}

std::vector<Task> readRecords(const std::string &filepath)
{
    std::ifstream file(filepath);
    if (!file.is_open())
    {
        std::cerr << "Error: Unable to open the file." << std::endl;
        std::exit(1);
    }
    // assertFunction(file.is_open(), []() { std::cerr << "Error: Unable to open the file." << std::endl; });

    std::vector<Task> records;
    std::string line;
    while (std::getline(file, line))
    {
        std::istringstream iss(line);
        int R, T;
        if (!(iss >> R >> T))
        {
            std::cerr << "Error: Invalid file format." << std::endl;
            std::exit(1);
        }
        records.push_back({R, T});
    }

    return records;
}

std::vector<Task> sortRecords(const std::vector<Task> &records)
{
    auto minmax = std::minmax_element(records.begin(), records.end(), [](const Task &a, const Task &b)
                                      { return a.T < b.T; });

    int min = minmax.first->T;
    int max = minmax.second->T;

    std::vector<std::vector<Task>> recordBuckets(max - min + 1);

    for (const auto &record : records)
    {
        recordBuckets[record.T - min].push_back(record);
    }

    std::vector<Task> sortedRecords;
    for (int i = recordBuckets.size() - 1; i >= 0; --i)
    {
        sortedRecords.insert(sortedRecords.end(), recordBuckets[i].begin(), recordBuckets[i].end());
    }

    return sortedRecords;
}

int main(int argc, char *argv[])
{
    std::string filepath;
    int algo = 0;
    int rn = 0;
    bool prettyPrint = false;
    bool secondTask = false;
    bool thirdTask = false;
    bool fourthTask = false;

    // Парсинг аргументов командной строки
    for (int i = 1; i < argc; ++i)
    {
        std::string arg(argv[i]);
        if (arg == "-f" && i + 1 < argc)
        {
            filepath = argv[++i];
        }
        else if (arg == "-nfdh")
        {
            algo = 1;
        }
        else if (arg == "-ffdh")
        {
            algo = 2;
        }
        else if (arg == "-n" && i + 1 < argc)
        {
            rn = std::stoi(argv[++i]);
        }
        else if (arg == "-p")
        {
            prettyPrint = true;
        }
        else if (arg == "-2task")
        {
            secondTask = true;
        }
        else if (arg == "-3task")
        {
            thirdTask = true;
        }
        else if (arg == "-4task")
        {
            fourthTask = true;
        }
        else
        {
            std::cerr << "Error: Invalid command-line arguments." << std::endl;
            std::exit(1);
        }
    }

    // Проверка аргументов
    if (filepath.empty())
    {
        std::cerr << "Error: Path should be set." << std::endl;
        std::exit(1);
    }

    if (algo == 0)
    {
        std::cerr << "Error: One of the algorithm flags (-nfdh or -ffdh) is required." << std::endl;
        std::exit(1);
    }

    if (rn <= 0)
    {
        std::cerr << "Error: Number of elementary machines is either not set or is not positive." << std::endl;
        std::exit(1);
    }

    // Чтение записей из файла
    auto records = readRecords(filepath);

    // Сортировка записей
    auto sortedRecords = sortRecords(records);

    // Выбор алгоритма в соответствии с флагом
    auto algoFunction = [&sortedRecords, rn](int algoFlag) -> std::vector<std::vector<Task>> (*)(const std::vector<Task> &, int)
    {
        switch (algoFlag)
        {
        case 1: // NFDH
            return NFDH;
        case 2: // FFDH
            return FFDH;
        default:
            std::cerr << "Error: Invalid algorithm flag." << std::endl;
            std::exit(1);
        }
    }(algo);

    // Вывод результатов
    auto oae = outputAlgoEfficiency(sortedRecords, rn, algoFunction);
    if (secondTask)
    {
        std::cout << oae.PerformanceTime;
    }
    if (thirdTask)
    {
        std::cout << " " << oae.ScheduleTotalTime;
    }
    if (fourthTask)
    {
        std::cout << " " << oae.ScheduleDeviation;
    }

    switch (algo)
    {
    case 1: // NFDH
        if (oae.Schedule.empty())
        {
            std::cerr << "Error: NFDH algorithm failed." << std::endl;
        }
        break;
    case 2: // FFDH
        if (oae.Schedule.empty())
        {
            std::cerr << "Error: FFDH algorithm failed." << std::endl;
        }
        break;
    default:
        std::cerr << "Error: Invalid algorithm flag." << std::endl;
        std::exit(1);
    }

    return 0;
}