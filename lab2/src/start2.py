import subprocess
from os import system, chdir
import random

class Lab2_1():
    algo_list = [
        ('FFDH', 'First Fit Decreasing Height', '-ffdh'),
        ('NFDH', 'Next Fit Decreasing Height', '-nfdh')
    ]
    mr = range(500, 5001, 500)
    tj = range(1, 101)

    @staticmethod
    def buildRandomTasks(n: int, path: str):
        rj = range(1, n+1)
        for m in Lab2_1.mr:
            with open(path, 'w') as f:
                for _ in range(m):
                    print(f'{random.choice(rj)} {random.choice(Lab2_1.tj)}', file=f)
            yield m

    @staticmethod
    def run(n: int):
        total = ""
        if n <= 0:
            raise Exception(f'n must be positive (got {n} in args)')
        random.seed(n)
        for m in Lab2_1.buildRandomTasks(n, 'tasks.csv'):
            total += str(m) + " "
            for algo in Lab2_1.algo_list:
                res = subprocess.run(["./main", algo[2], "-n", "1024", "-f", "tasks.csv", "-2task"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                if res.returncode != 0:
                    print(f'Error running algorithm {algo[0]} for n={n}: {res.stderr}')
                else:
                    stdout = res.stdout.decode('utf-8')
                    total += stdout + " "
                res = subprocess.run(["./main", algo[2], "-n", "4096", "-f", "tasks.csv", "-2task"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                if res.returncode != 0:
                    print(f'Error running algorithm {algo[0]} for n={n}: {res.stderr}')
                else:
                    stdout = res.stdout.decode('utf-8')
                    total += stdout + " "
            total += "\n"
        with open("bench/bench.csv", "w+") as file:
            file.writelines(total)

class Lab2_2:
    algo_list = [
        ('FFDH', 'First Fit Decreasing Height', '-ffdh'),
        ('NFDH', 'Next Fit Decreasing Height', '-nfdh')
    ]
    mr = range(500, 5001, 500)
    tj = range(1, 101)

    @staticmethod
    def buildRandomTasks(n: int, path: str):
        rj = range(1, n+1)
        for m in Lab2_2.mr:
            with open(path, 'w') as f:
                for _ in range(m):
                    print(f'{random.choice(rj)} {random.choice(Lab2_2.tj)}', file=f)
            yield m

    @staticmethod
    def run(n: int):
        total = ""
        if n <= 0:
            raise Exception(f'n must be positive (got {n} in args)')
        random.seed(n)
        for m in Lab2_2.buildRandomTasks(n, 'tasks.csv'):
            total += str(m)
            for algo in Lab2_2.algo_list:
                res = subprocess.run(["./main", algo[2], "-n", "1024", "-f", "tasks.csv", "-3task"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                if res.returncode != 0:
                    print(f'Error running algorithm {algo[0]} for n={n}: {res.stderr}')
                else:
                    stdout = res.stdout.decode('utf-8')
                    total += stdout
                res = subprocess.run(["./main", algo[2], "-n", "4096", "-f", "tasks.csv", "-3task"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                if res.returncode != 0:
                    print(f'Error running algorithm {algo[0]} for n={n}: {res.stderr}')
                else:
                    stdout = res.stdout.decode('utf-8')
                    total += stdout
            total += "\n"
        with open("bench/bench3.1.csv", "w+") as file:
            file.writelines(total)

class Lab2_3:
    algo_list = [
        ('FFDH', 'First Fit Decreasing Height', '-ffdh'),
        ('NFDH', 'Next Fit Decreasing Height', '-nfdh')
    ]
    mr = range(500, 5001, 500)
    tj = range(1, 101)

    @staticmethod
    def buildRandomTasks(n: int, path: str):
        rj = range(1, n+1)
        for m in Lab2_3.mr:
            with open(path, 'w') as f:
                for _ in range(m):
                    print(f'{random.choice(rj)} {random.choice(Lab2_3.tj)}', file=f)
            yield m

    @staticmethod
    def run(n: int):
        total = ""
        if n <= 0:
            raise Exception(f'n must be positive (got {n} in args)')
        random.seed(n)
        for m in Lab2_3.buildRandomTasks(n, 'tasks.csv'):
            total += str(m)
            for algo in Lab2_3.algo_list:
                res = subprocess.run(["./main", algo[2], "-n", "1024", "-f", "tasks.csv", "-4task"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                if res.returncode != 0:
                    print(f'Error running algorithm {algo[0]} for n={n}: {res.stderr}')
                else:
                    stdout = res.stdout.decode('utf-8')
                    total += stdout
                res = subprocess.run(["./main", algo[2], "-n", "4096", "-f", "tasks.csv", "-4task"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                if res.returncode != 0:
                    print(f'Error running algorithm {algo[0]} for n={n}: {res.stderr}')
                else:
                    stdout = res.stdout.decode('utf-8')
                    total += stdout
            total += "\n"
        with open("bench/bench3.2.csv", "w+") as file:
            file.writelines(total)

class Lab2_4:
    algo_list = [
        ('FFDH', 'First Fit Decreasing Height', '-ffdh'),
        ('NFDH', 'Next Fit Decreasing Height', '-nfdh')
    ]
    mr = range(500, 5001, 500)
    tj = range(1, 101)

    @staticmethod
    def run(n: int, tasks_path: str):
        total = ""
        if n <= 0:
            raise Exception(f'n must be positive (got {n} in args)')
        random.seed(n)
        for m in Lab2_4.mr:
            total += str(m)
            res = subprocess.run(["./gen", str(m)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            for algo in Lab2_4.algo_list:
                res = subprocess.run(["./main", algo[2], "-n", "1024", "-f", "tasks.csv", "-3task"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                stdout = res.stdout.decode('utf-8')
                total += stdout
            total += "\n"
        with open("bench/bench4.1.csv", "w+") as file:
            file.writelines(total)

class Lab2_5:
    algo_list = [
        ('FFDH', 'First Fit Decreasing Height', '-ffdh'),
        ('NFDH', 'Next Fit Decreasing Height', '-nfdh')
    ]
    mr = range(500, 5001, 500)
    tj = range(1, 101)

    @staticmethod
    def run(n: int, tasks_path: str):
        total = ""
        if n <= 0:
            raise Exception(f'n must be positive (got {n} in args)')        
        for m in Lab2_5.mr:
            total += str(m)
            res = subprocess.run(["./gen", str(m)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout = res.stdout.decode('utf-8')
            total += stdout
            for algo in Lab2_5.algo_list:
                res = subprocess.run(["./main", algo[2], "-n", "1024", "-f", "tasks.csv", "-4task"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                stdout = res.stdout.decode('utf-8')
                total += stdout
            total += "\n"
        with open("bench/bench4.2.csv", "w+") as file:
            file.writelines(total)

#def lab2():
    #chdir("../lab2/src")
system("g++ tasksgen.cpp -o ./gen")
system("g++ main.cpp -o main")
Lab2_1.run(1024)
Lab2_2.run(1024)
Lab2_3.run(1024)
Lab2_4.run(1024, "tasks.csv")
Lab2_5.run(1024, "tasks.csv")
system("gnuplot gnuplot/task2.gnuplot")
system("gnuplot gnuplot/task3.1.gnuplot")
system("gnuplot gnuplot/task3.2.gnuplot")
system("gnuplot gnuplot/task4.1.gnuplot")
system("gnuplot gnuplot/task4.2.gnuplot")
system("rm main")
system("rm gen")
system("rm tasks.csv")
    #system("rm -rf __pycache__")
system("rm bench/bench.csv")
system("rm bench/bench3.1.csv")
system("rm bench/bench3.2.csv")
system("rm bench/bench4.1.csv")
system("rm bench/bench4.2.csv")
