set terminal png size 1200, 900 font 'Verdana, 14'
set title "Лабораторная работа 2. Задание №3: Среднеквадратичное отклонение" font "Helvetica Bold, 18"
set output "output/task3.2.png"

set grid
set key left top

set xrange [*:*]
set ylabel "Среднеквадратичное отклонение"

set yrange [*:*]
set xlabel "Количество задач"

plot 'bench/bench3.2.csv' u 1:2 with linespoints lw 3 pt 7 ps 1 title 'FFDH, n = 1024',\
     'bench/bench3.2.csv' u 1:4 with linespoints lw 3 pt 7 ps 1 title 'NFDH, n = 1024'