#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <cassert>
#include <iomanip>
#include <stdexcept>

int computeTime(const std::string &time)
{
    auto mount = std::stoi(time.substr(0, 2));
    auto days = std::stoi(time.substr(3, 2));
    auto hours = std::stoi(time.substr(6, 2));
    auto minutes = std::stoi(time.substr(9, 2));
    auto seconds = std::stoi(time.substr(12, 2));
    auto total_seconds =
        mount * 2592000 + days * 86400 + hours * 3600 + minutes * 60 + seconds;
    return total_seconds;
}

void parseLog(
    const std::string &logPath,
    const std::string &parsedPath,
    const std::size_t &stringCount)
{
    // Переменная для файла(чтение с лога)
    std::ifstream logFile;
    // Переменная для паршенного файла
    std::ofstream parsedFile;
    // Открытие файлов
    parsedFile.open(parsedPath);
    logFile.open(logPath);

    std::string read_string;
    std::size_t counter = 0;
    // Чтение из файла лога
    while (std::getline(logFile, read_string))
    {
        // Как только считали заданное количество
        if (counter == stringCount)
        {
            break;
        }
        // Токенизация строки
        std::vector<std::string> tokens;
        std::string token;
        std::istringstream text_stream(read_string);
        while (text_stream >> token)
        {
            tokens.emplace_back(token);
        }
        std::string parsedString;
        // Вычисление времени выполнения задачи
        auto beginTime = computeTime(
            tokens.at(6).substr(tokens.at(6).find_first_of('=') + 1));
        auto endTime = computeTime(
            tokens.at(7).substr(tokens.at(7).find_first_of('=') + 1));
        auto elapsedTime = endTime - beginTime;
        // Количество узлов
        auto nodeCount =
            tokens.at(9).substr(tokens.at(9).find_first_of('=') + 1);

        if (std::stoi(nodeCount) > 0 && elapsedTime > 0)
        {
            // Запись в паршенный файл
            parsedFile << nodeCount << " " << elapsedTime << "\n";
            counter++;
        }
    }
    // Закрытие файлов
    parsedFile.close();
    logFile.close();
}

int main(int argc, char *argv[])
{
    parseLog("LLNL-Thunder-2007-0.txt", "tasks.csv", std::atoi(argv[1]));
    return 0;
}